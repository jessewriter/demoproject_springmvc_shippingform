package com.jits.core.carrier;

 
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jits.core.DeliveryPriorityType;
import com.jits.core.carrier.PostalCarrier;


@RunWith(SpringJUnit4ClassRunner.class)
//specifies the Spring configuration to load for this test fixture
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/root-context.xml"})
public class CarrierTest {
	
	@Autowired
	@Qualifier("uex")
	private PostalCarrier unitedExpress;
	
	@Autowired 
	@Qualifier("fps")
	private PostalCarrier federalPostalService;
 
	@Before 
	public void setUp(){
 	 
	}
	
	@Test
	public void testUnitedExpress(){
		double first =  unitedExpress.getTariff(DeliveryPriorityType.FIRST);
		assertEquals("FIRST", 0.3 , first, 0.001);
		double second =  unitedExpress.getTariff(DeliveryPriorityType.SECOND);
		assertEquals("SECOND", 0.15 , second, 0.001);
		double third =  unitedExpress.getTariff(DeliveryPriorityType.WHENEVER);
		assertEquals("WHENEVER", 0.05 , third, 0.001);	
	}
	 
	@Test
	public void testFederalPostalService(){
		double first =  federalPostalService.getTariff(DeliveryPriorityType.FIRST);
		assertEquals("FIRST", 0.44 , first, 0.001);
		double second =  federalPostalService.getTariff(DeliveryPriorityType.SECOND);
		assertEquals("SECOND", 0.15 , second, 0.001);
		double third =  federalPostalService.getTariff(DeliveryPriorityType.WHENEVER);
		assertEquals("WHENEVER", 0.02 , third, 0.001);	
	} 
}
