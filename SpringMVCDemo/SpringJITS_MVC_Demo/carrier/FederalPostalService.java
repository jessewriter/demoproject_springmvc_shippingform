package com.jits.core.carrier;

 
import java.util.Map;

import com.jits.core.DeliveryPriorityType;

public class FederalPostalService extends PostalCarrier {
	
	 
	FederalPostalService(Map<String,Tariff> tariffs, String name){
		super(tariffs, name);
	}
	
	public double getTariff(DeliveryPriorityType dType) {		 
			double priority = 0;
			switch(dType){
			  case FIRST: priority = this.getTariffs().get("EXPRESS").getAmount(); break;
			  case SECOND: priority = this.getTariffs().get("PRIORITY").getAmount(); break;
			  default: priority = this.getTariffs().get("REG").getAmount();
			}
			return priority;
		 
	}
	
	public DeliveryPriorityType getTariff(String type) {		 
		DeliveryPriorityType priority = DeliveryPriorityType.WHENEVER;
		if(type.equals("EXPRESS")){
		  priority = DeliveryPriorityType.FIRST;
		}
		if(type.equals("PRIORITY")){
			  priority = DeliveryPriorityType.SECOND;
			}
			
		return priority;	 
     }
	 
	
	

}
