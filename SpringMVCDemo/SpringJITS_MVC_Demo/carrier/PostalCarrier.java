package com.jits.core.carrier;

 
import java.util.Map;

 

public abstract class PostalCarrier implements Carrier{
	
	 
	
	private Map<String,Tariff> tariffs;
	private String name;
	
	PostalCarrier(){}
 	
	PostalCarrier(Map<String,Tariff> tariffs, String name){
		this.tariffs = tariffs;
		this.name = name;
	}

	public Map<String, Tariff> getTariffs() {
		return tariffs;
	}

	public void setTariffs(Map<String, Tariff> tariffs) {
		this.tariffs = tariffs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	 
	 
}
