package com.jits.core.carrier;

 
import java.util.Map;

import com.jits.core.Delivery;
import com.jits.core.DeliveryPriorityType;
import com.jits.core.DeliveryType;

public class UnitedExpress extends PostalCarrier {

	UnitedExpress(Map<String,Tariff> tariffs, String name) {
		super(tariffs, name);
	}

	public double getTariff(DeliveryPriorityType dType) {
		double priority = 0;
		switch(dType){
		  case FIRST: priority = this.getTariffs().get("FRST").getAmount(); break;
		  case SECOND: priority = this.getTariffs().get("SEC").getAmount(); break;
		  default: priority = this.getTariffs().get("OTHER").getAmount();
		}
		return priority;
	}

	public DeliveryPriorityType getTariff(String type) {		 
		DeliveryPriorityType priority = DeliveryPriorityType.WHENEVER;
		if(type.equals("FRST")){
		  priority = DeliveryPriorityType.FIRST;
		}
		if(type.equals("SEC")){
			  priority = DeliveryPriorityType.SECOND;
			}
			
		return priority;	 
     } 

}
