package com.jits.core.carrier;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CarrierConfig {
	@Resource(name="federalMap")
	private Map<String, Tariff> tariffs;
	
	@Bean(name="fps")
	public Carrier stuff(){
		Carrier c = new FederalPostalService(tariffs, "FederalPostalService Carrier Old");
		return c;
	}
	
	@Resource(name="unitedExpressMap")
	private Map<String, Tariff> moreTariffs;
	
	@Bean(name="uex")
	public Carrier stuffTwo(){
		Carrier c = new UnitedExpress(moreTariffs, "United Express carrier old");
		return c;
	}
	
	@Bean(name="federalMap")
	Map<String, Tariff> federalMap(){
		Map<String, Tariff> federalMap = new HashMap<String, Tariff>();
		federalMap.put("EXPRESS", new TariffImpl("EXPRESS", 0.44, "24HRS"));
		federalMap.put("PRIORITY", new TariffImpl("PRIORITY", 0.15, "1-3 Business Days"));
		federalMap.put("REGULAR", new TariffImpl("REGULAR", 0.02, "4-5 Business Days"));
		return federalMap;
	}
	
	@Bean(name="federalPSBean")// inject federalMap() bean as an arg
	FederalPostalService fedPS(){
		return new FederalPostalService(federalMap(), "Federal Postal Service");
	}
	
	@Bean(name="unitedExpressMap")
	Map<String, Tariff> ueMap(){
		Map<String, Tariff> ueMap = new HashMap<String, Tariff>();
		ueMap.put("FRST", new TariffImpl("FRST", 0.30, "48HRS"));
		ueMap.put("SEC", new TariffImpl("SEC", 0.15, "1-3 Business Days"));
		ueMap.put("OTHER", new TariffImpl("OTHER", 0.05, "5-10 Business Days"));
		return ueMap;
	}
	
	@Bean(name="unitedExpressBean")
	UnitedExpress unitedExpress(){
		return new UnitedExpress(ueMap(), "United Express");
	}
	

}
