package com.jits.core.carrier;

 
import java.util.Map;

import com.jits.core.DeliveryPriorityType;

public class FederalPostalService extends PostalCarrier {
	 
	FederalPostalService(Map<String,Tariff> tariffs, String name){
		super(tariffs, name);
	}
	
	public double retrieveTariffFactor(DeliveryPriorityType dType) {		 
			double teriffFactor = 0;
			switch(dType){
			  case FIRST: teriffFactor = this.getTariffs().get("EXPRESS").getFactor(); break;
			  case SECOND: teriffFactor = this.getTariffs().get("PRIORITY").getFactor(); break;
			  default: teriffFactor = this.getTariffs().get("REGULAR").getFactor();
			}
			return teriffFactor;
		 
	}
	
	public DeliveryPriorityType retrieveDeliveryPriorityType(String type) {		 
		DeliveryPriorityType priority = DeliveryPriorityType.WHENEVER;
		if(type.equals("EXPRESS")){
		  priority = DeliveryPriorityType.FIRST;
		}
		if(type.equals("PRIORITY")){
			  priority = DeliveryPriorityType.SECOND;
			}
			
		return priority;	 
     }


	 
 
	 
	
	

}
