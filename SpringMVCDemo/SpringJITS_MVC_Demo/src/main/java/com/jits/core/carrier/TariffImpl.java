package com.jits.core.carrier;

 

public class TariffImpl implements Tariff {
	
	private String code;
	private double amount;
	private String duration;
	
	TariffImpl(String code, double amount, String duration){
		this.code = code;
		this.amount = amount;
		this.duration = duration;
	}

	public double getFactor() {
		// TODO Auto-generated method stub
		return amount;
	}

	public String getCode() {
		return code;
	}

	public String getDuration() {
		return duration;
	}
	
	 

}
