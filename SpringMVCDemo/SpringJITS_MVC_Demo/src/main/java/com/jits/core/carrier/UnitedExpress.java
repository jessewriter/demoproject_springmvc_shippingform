package com.jits.core.carrier;

 
import java.util.Map;

import com.jits.core.DeliveryPriorityType;

public class UnitedExpress extends PostalCarrier {

	UnitedExpress(Map<String,Tariff> tariffs, String name) {
		super(tariffs, name);
	}

	public double retrieveTariffFactor(DeliveryPriorityType dType) {
		double priority = 0;
		switch(dType){
		  case FIRST: priority = this.getTariffs().get("FRST").getFactor(); break;
		  case SECOND: priority = this.getTariffs().get("SEC").getFactor(); break;
		  default: priority = this.getTariffs().get("OTHER").getFactor();
		}
		return priority;
	}

	public DeliveryPriorityType retrieveDeliveryPriorityType(String type) {		 
		DeliveryPriorityType priority = DeliveryPriorityType.WHENEVER;
		if(type.equals("FRST")){
		  priority = DeliveryPriorityType.FIRST;
		}
		if(type.equals("SEC")){
			  priority = DeliveryPriorityType.SECOND;
			}
			
		return priority;	 
     }

	@Override
	public String toString() {
		return "UnitedExpress [getTariffs()=" + getTariffs() + ", getName()="
				+ getName() + "]";
	}

	 

}
