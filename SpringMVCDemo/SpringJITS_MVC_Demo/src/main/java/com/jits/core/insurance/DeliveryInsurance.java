package com.jits.core.insurance;

public class DeliveryInsurance implements Insurable {
	
	private double factor;
	private String description;

	public double addInsurance(double currentCost) {
		return currentCost * factor;
	}

	public double getFactor() {
		return factor;
	}

	public String getDescription() {
		return description;
	}

	public void setFactor(double factor) {
		this.factor = factor;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "DeliveryInsurance [factor=" + factor + ", description="
				+ description + "]";
	}
	
	

}
