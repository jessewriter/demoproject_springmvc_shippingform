package com.jits.core.kiosk;

import java.io.FileNotFoundException;
import java.util.List;

import com.corporate.core.Discount;
import com.jits.core.Confirmation;
import com.jits.core.JitsDelivery;
import com.jits.core.carrier.Carrier;
import com.jits.core.insurance.Insurable;
import com.jits.web.DeliveryPO;

public interface Kiosk {
	
	public List<Carrier> getCarriers();
	public List<Discount> getDiscounts();
	public Discount getDiscount(String key);
	public JitsDelivery createJitsDelivery(DeliveryPO deliveryIn);
	public void addInsuranceOption(String option);
	public Confirmation confirm() throws FileNotFoundException;
	//5
	List<Insurable> getInsurables();
	JitsDelivery getDelivery();
	Confirmation getConfirmation();
	
	Carrier getCarrier(String carrierName);
	Carrier getSelectedCarrier();
	
}
