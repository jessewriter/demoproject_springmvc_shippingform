package com.jits.core.kiosk;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.context.WebApplicationContext;

import com.corporate.core.Discount;
import com.jits.core.Address;
import com.jits.core.Confirmation;
import com.jits.core.DeliveryPriorityType;
import com.jits.core.DeliveryTO;
import com.jits.core.DeliveryType;
import com.jits.core.JitsDelivery;
import com.jits.core.JitsDeliveryFactory;
import com.jits.core.carrier.Carrier;
import com.jits.core.insurance.Insurable;
import com.jits.web.DeliveryPO;
//@Component // not needed filter included in xml
@Scope(value="session", proxyMode=ScopedProxyMode.INTERFACES)
public class KioskImp implements Kiosk {
	
	@Autowired
	private WebApplicationContext context; 
	@Resource(name="jaxb2Marshaller")
	private Jaxb2Marshaller jaxb2Marshaller;
	
	@Resource(name="filteredInsList")
	private List<Insurable> insurables;
	
	@Autowired
	private List<Carrier> carriers;
	@Autowired  // from corporate context
	private List<Discount> discounts;
	private JitsDelivery jitsDelivery;
	private Carrier selectedCarrier;
	
	private Confirmation confirmation;
	
	public JitsDelivery createJitsDelivery(DeliveryPO deliveryIn){
		Discount discount = findCoupon(deliveryIn.getDiscountCode());
		selectedCarrier =  getCarrier(deliveryIn.getCarrier());
		DeliveryType type = DeliveryType.valueOf(deliveryIn.getType());
		// this req to translate unique priority to kiosk compatible DPT
		DeliveryPriorityType priority = selectedCarrier.retrieveDeliveryPriorityType(deliveryIn.getPriority());
		
		DeliveryTO to = new DeliveryTO();
		to.setCarrier(selectedCarrier);
		to.setDiscount(discount);
		to.setPriority(priority);
		to.setType(type);
		to.setWeight(deliveryIn.getWeight());
		
		Address origin = new Address(deliveryIn.getOriginStreet(), deliveryIn.getOriginCity(), deliveryIn.getOriginState(), deliveryIn.getOriginZipcode());
		Address destination = new Address(deliveryIn.getDestStreet(), deliveryIn.getDestCity(), deliveryIn.getDestState(), deliveryIn.getDestZipcode());
		to.setDestination(destination);
		to.setOrigin(origin);
		
		jitsDelivery = 	 JitsDeliveryFactory.createDeliverable(to); 
		return jitsDelivery;
		
	}
	private Discount findCoupon(String code){
		Discount result = null;
		List<Discount> discounts = this.getDiscounts(); 
		for (Discount discount : discounts) {
			if(discount.getCode().equals(code));{
				result = discount;
				break;
			}
		} 
		return result;
	} 
	
	public void addInsuranceOption(String option) {
		for (Insurable element : insurables) {
			if(element.getDescription().equals(option)){
				jitsDelivery.setInsurance(element);
			}
		}
		jitsDelivery.calculateCost();
	}
	

	public List<Carrier> getCarriers() {
		return carriers;
	}

	public List<Discount> getDiscounts() {
		return discounts;
	}

	public Discount getDiscount(String key) {
		Discount result = null;
		for (Discount discount : discounts) {
			if(discount.getCode().equals(key)){
				result = discount;
				break;
			}
		}
		return result;
	}

	public void setFilteredInsList(List<Insurable> filteredInsList) {
		this.insurables = filteredInsList;
	}
	
	@Override
	public Confirmation confirm() throws FileNotFoundException {
		Confirmation confirmation = jitsDelivery.confirm();
		marshalConfirmation(confirmation);
		jitsDelivery = null;
		setConfirmation(confirmation);
		return confirmation;
	}
	
	private void marshalConfirmation(Confirmation confirmation) throws FileNotFoundException {
		// must create destination folder first or io errors
		//String destination = "xml/" +confirmation.getTrackingNumber() + ".xml";
		//lab 7 we need to find in the tc server
		String absFileSysPath = context.getServletContext().getRealPath("/");
		File file = new File(absFileSysPath + "/xml/" + 
		confirmation.getTrackingNumber() + ".xml"		);
		file.getParentFile().mkdirs();  // will create dirs if not existing
		StreamResult result = new StreamResult(new FileOutputStream(file));
		// when writing directory in linux not / needed ie just test/ not /test/
		jaxb2Marshaller.marshal(confirmation, result);
	}
	public Confirmation getConfirmation() {
		return confirmation;
	}
	public void setConfirmation(Confirmation confirmation) {
		this.confirmation = confirmation;
	}
	@Override
	public List<Insurable> getInsurables() {
		return insurables;
	}
	@Override
	public JitsDelivery getDelivery() {
		return jitsDelivery;
	}
	@Override
	public Carrier getCarrier(String carrierName) {
		Carrier result = null;
		List<Carrier> carriers = this.getCarriers(); 
		for (Carrier carrier : carriers) {
			if(carrier.getName().equals(carrierName)){
				result = carrier;
			}
		}
		selectedCarrier = result;
		return result;
	}
	@Override
	public Carrier getSelectedCarrier() {
		return selectedCarrier;
	}



}
