package com.jits.processor;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.corporate.core.Discount;
//@Component filter include in xml
public class CouponProcessor implements BeanPostProcessor, ApplicationContextAware{
	
	@Resource
	private ApplicationContext context;
	
	@Resource(name="discountCodeXref")
	Map<String, String> discountCodeXref;

	public Object postProcessAfterInitialization(Object objIn, String arg1)
			throws BeansException {
		String codeOut = null;
		// if discount adapt to proper discount code using map
		if(objIn instanceof Discount){
			String codeIn = ((Discount) objIn).getCode();
			if(discountCodeXref.containsKey(codeIn)){
				codeOut = discountCodeXref.get(codeIn);
			((Discount) objIn).setCode(discountCodeXref.get(codeOut));
			}
			context.publishEvent(new DiscountEvent(codeIn + "Discount Code Updated! to " + codeOut));
		}
		return objIn;
	}

	public Object postProcessBeforeInitialization(Object objIn, String arg1)
			throws BeansException {
		// TODO Auto-generated method stub
		return objIn;
	}

	public void setApplicationContext(ApplicationContext contextIn)
			throws BeansException {
		context = contextIn;
	}

}
