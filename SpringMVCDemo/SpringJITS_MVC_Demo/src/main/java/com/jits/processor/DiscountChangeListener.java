package com.jits.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.web.context.WebApplicationContext;

public class DiscountChangeListener implements ApplicationListener<DiscountEvent> {

	@Autowired
	private WebApplicationContext context;
	
	public void onApplicationEvent(DiscountEvent discountEventIn) {
		PrintWriter pw;
		try {
			String aboluteFileSystemPath = context.getServletContext().getRealPath("/");
			File f = new File(aboluteFileSystemPath + "discountLogWeb.txt"); 
// /home/cat/Desktop/sts-bundle/pivotal-tc-server-developer-3.1.0.RELEASE/base-instance/wtpwebapps/PL4_Lab05_MVC
//			pw = new PrintWriter(new FileOutputStream("discountLog.txt", true));
			pw = new PrintWriter(new FileOutputStream(f));
			pw.println(discountEventIn.getSource());
			pw.close();
			System.out.println("printed out discount event into tc server location");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		}

}
