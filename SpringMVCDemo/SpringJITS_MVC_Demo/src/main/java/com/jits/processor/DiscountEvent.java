package com.jits.processor;

import org.springframework.context.ApplicationEvent;

public class DiscountEvent extends ApplicationEvent {
	private static final long serialVersionUID = 1039681617062011347L;

	public DiscountEvent(Object source) {
		super(source);

	}

}
