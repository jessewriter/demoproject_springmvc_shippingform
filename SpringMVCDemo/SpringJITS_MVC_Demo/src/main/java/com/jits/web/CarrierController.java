package com.jits.web;

import java.io.FileNotFoundException;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import com.jits.core.Confirmation;
import com.jits.core.carrier.Carrier;
import com.jits.core.kiosk.Kiosk;

@Controller
@RequestMapping(value = "/carrier")
public class CarrierController {

	@Autowired
	private WebApplicationContext context;

	@Autowired
	// KioskImp kioskImp;
	Kiosk kiosk; // use interface

	@ModelAttribute(value = "carriers")
	public List<Carrier> getCarriers() {
		return kiosk.getCarriers();
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String goHome() {
		return "home";
	}

	@RequestMapping(value = "/selectCarrier", method = RequestMethod.GET)
	public String selectCarrier() {
		return "carrier";
	}

	@RequestMapping(value = "/selectCarrier", method = RequestMethod.POST)
	// public ModelAndView selectedCarrier(HttpServletRequest request){
	public ModelAndView selectedCarrier(@RequestParam("submit") String request) {
		// kiosk.getCarrier(request.getParameter("submit"));
		// kiosk.getCarrier(request);
		Carrier c = kiosk.getCarrier(request);
		ModelAndView modelAndView = new ModelAndView();
		// modelAndView.setViewName("redirect:createDelivery");
		modelAndView.setViewName("inputDelivery");
		modelAndView.addObject("carrier", c);

		// lab 6
		DeliveryPO po = new DeliveryPO();
		po.setCarrier(c.getName());
		modelAndView.addObject("deliveryPO", po);
		return modelAndView;
	}

	/*
	 * POST for Lab06
	 */

	@Resource
	private RouteValidator routeValidator;

	@RequestMapping(value = "/createDelivery", method = RequestMethod.POST)
	public ModelAndView addInsurance(
			@Valid @ModelAttribute DeliveryPO deliveryPO, BindingResult result) {
		routeValidator.validate(deliveryPO, result);
		if (result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.addObject("carrier", kiosk.getSelectedCarrier());
			modelAndView.setViewName("inputDelivery");
			return modelAndView;
		} else {
			kiosk.createJitsDelivery(deliveryPO);
			return new ModelAndView("redirect:/carrier/addInsurance");
		}
	}

	/*
	 * GET for insure
	 */

	@RequestMapping(value = "/addInsurance", method = RequestMethod.GET)
	public ModelAndView addInsurance(@ModelAttribute Command command) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("insurance");
		mav.addObject("delivery", kiosk.getDelivery());
		mav.addObject("command", command);
		mav.addObject("insurables", kiosk.getInsurables());
		// remember not to create a new model and view !
		return mav;
	}

	@RequestMapping(value = "/addInsurance", method = RequestMethod.POST)
	public ModelAndView submitInsurance(@ModelAttribute Command command) {
		kiosk.addInsuranceOption(command.getValue());
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:getConfirmation");
		return mav;
	}

	@RequestMapping(value = "/getConfirmation", method = RequestMethod.GET)
	public ModelAndView getConfirm(@ModelAttribute Command command) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("delivery", kiosk.getDelivery());
		mav.setViewName("confirm");
		return mav;
	}

	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
	public ModelAndView confirm() {
		Confirmation confirmation = null;
		try {
			confirmation = kiosk.confirm();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView();
		mv.addObject("confirm", confirmation);
		mv.setViewName("end");
		return mv;
	}

	@RequestMapping(value = "/createJits", method = RequestMethod.GET)
	public ModelAndView createJits(@ModelAttribute("command") List<Carrier> c) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("carrier");
		modelAndView.addObject("carriers", getCarriers());
		return modelAndView;
	}

	@RequestMapping(value="/pdf", method = RequestMethod.GET)
	public ModelAndView createPDF(){
		ModelAndView mav = new ModelAndView();
		mav.addObject("confirmation", kiosk.getConfirmation());
		mav.setViewName("pdf");
		return mav;
	}
	
	/*
	 * Get and Post for input delivery NOT needed
	 */
	// @RequestMapping(value="/createDelivery", method = RequestMethod.GET)
	// public ModelAndView buildDeliveryPage(HttpServletRequest request){
	// ModelAndView modelAndView = new ModelAndView();
	// modelAndView.addObject("carrier", kiosk.getSelectedCarrier());
	// modelAndView.setViewName("inputDelivery");
	// return modelAndView;
	// }
	@RequestMapping(value = "/path", method = RequestMethod.GET)
	public String getPath() {
		String aboluteFileSystemPath = context.getServletContext().getRealPath(
				"/");
		System.out.println("path = " + aboluteFileSystemPath);
		return "home";
	}
	// path =
	// /home/cat/Desktop/sts-bundle/pivotal-tc-server-developer-3.1.0.RELEASE/base-instance/wtpwebapps/PL4_Lab05_MVC/
}
