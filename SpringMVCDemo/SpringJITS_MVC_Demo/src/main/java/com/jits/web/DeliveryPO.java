package com.jits.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope("prototype")  // unique bean each instance
public class DeliveryPO {
 
	
	//lab 6 add validation checks
	@Size(min=1, message="Please enter an Origin Street")
	private String originStreet;
    @Size(min=1, message="Please enter an Origin City")
	private String originCity;
	@Size(min=2, max=2, message="Origin State must be 2 characters")
	private String originState;
	@Size(min=5, max=5, message="Origin Zip code must be 5 characters")
	private String originZipcode;
	@Size(min=1, message="Please enter a Destination Street")
	private String destStreet;
	@Size(min=1, message="Please enter a Destination City")
	private String destCity;
	@Size(min=2, max=2, message="Destination State must be 2 characters")
	private String destState;
	@Size(min=5, max=5, message="Destination Zip code must be 5 characters")
	private String destZipcode;
	@Size(min=3, message="Select a Delivery Type") 
	private String type;
	
	private double weight = 1.00;
	private String carrier;
	@Size(min=3, message="Select a Priority")
	private String priority;
	@NotNull(message="Please enter a Discount Code")
	private String discountCode;
	
	 

	public String getOriginStreet() {
		return originStreet;
	}

	public void setOriginStreet(String originStreet) {
		this.originStreet = originStreet;
	}

	public String getOriginCity() {
		return originCity;
	}

	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}

	public String getOriginState() {
		return originState;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}

	public String getOriginZipcode() {
		return originZipcode;
	}

	public void setOriginZipcode(String originZipcode) {
		this.originZipcode = originZipcode;
	}

	public String getDestStreet() {
		return destStreet;
	}

	public void setDestStreet(String destStreet) {
		this.destStreet = destStreet;
	}

	public String getDestCity() {
		return destCity;
	}

	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}

	public String getDestState() {
		return destState;
	}

	public void setDestState(String destState) {
		this.destState = destState;
	}

	public String getDestZipcode() {
		return destZipcode;
	}

	public void setDestZipcode(String destZipcode) {
		this.destZipcode = destZipcode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

 

}
