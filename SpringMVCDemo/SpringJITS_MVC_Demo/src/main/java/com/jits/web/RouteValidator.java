package com.jits.web;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

// make sure to import spring validator
@Component // i am a bean
public class RouteValidator implements Validator {
	
	@Resource
	private Map<String, String> discontinuedRoutes;

	public boolean supports(Class<?>  clazz) {
		return  DeliveryPO.class.equals(clazz);
	}

	public void validate(Object arg0, Errors errors) {
		 DeliveryPO po = ( DeliveryPO) arg0;
		 String origin = po.getOriginState();
		 String dest = po.getDestState();
		 System.out.println(discontinuedRoutes);
		 if(discontinuedRoutes.containsKey(origin)){
			 String dRoutes = (String) discontinuedRoutes.get(origin);
			 String[] destStates = dRoutes.split("-");
			 for (String string : destStates) {
				if(dest.equals(string)){
					errors.rejectValue("destState", null, "Carriers no longer service that route");
					break;
				}
			}
		 }
	}
}
