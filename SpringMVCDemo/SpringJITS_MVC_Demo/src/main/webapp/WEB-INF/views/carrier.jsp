<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JITS Choose Carrier</title>
</head>
<body>
<c:import url="Header.jsp" />
<h3>Select Carrier</h3>

	<c:forEach items="${carriers}" var="carrier">
	
	<form action="/web2/carrier/selectCarrier" method="post"> 
				    <input type="submit" value="${carrier.name}"  name="submit" 
				    class="carrierSubmit" 
				    onmouseover="document.getElementById('help').style.visibility='visible'"
				    onmouseout="document.getElementById('help').style.visibility='hidden'"
				    />
			  </form>
	
	<c:out value="Name: ${carrier.name}" />
  		<c:forEach items="${carrier.tariffs }" var="entry">
			<c:out value="Carrier code ${entry.value.code }" />
			<c:out value="Carrier code ${entry.value.duration }" />
		</c:forEach>

	</c:forEach>
	
	<div id="help" class="help">
         <SPAN>You need to click one of the green buttons to select a carrier</SPAN>
    </div> 
</body>
</html>