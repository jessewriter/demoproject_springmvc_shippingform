<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Confirmation</title>
</head>
<body>
<c:import url="Header.jsp" />
 <DIV><a href="addInsurance">Reset Insurance</a></DIV>
    <DIV class="subtitle" >Confirmation - ${delivery.carrier.name}</DIV>
 
    <DIV class="subtitle" >Current Cost: <fmt:formatNumber type="currency"  value="${delivery.cost}"/></DIV> 
    <DIV class="subtitle" >Priority: ${delivery.deliveryType }</DIV> 
    <DIV class="subtitle">Insurance: ${delivery.insurance.description }</DIV>
    <DIV class="subtitle">Discount: ${delivery.discount.code }</DIV>
    <DIV class="subtitle">Status: ${delivery.status }</DIV>
	 
	<form:form commandName="command" action="/web2/carrier/confirm" method="POST">
	    
	        <input type="submit" value="confirm" />
	     
	</form:form> 
</body>
</html>