<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:import url="Header.jsp" />
 <DIV class="subtitle" >Thank You For Using - ${confirm.carrierDescription}</DIV>
<a href="/web2/carrier/pdf">Print to PDF?</a>
    <DIV class="subtitle" >Final Cost: <fmt:formatNumber type="currency"  value="${confirm.cost}"/></DIV> 
    <DIV class="subtitle" >Weight: ${confirm.weight }</DIV> 
    <DIV class="subtitle">Status: ${confirm.status}</DIV>
     <DIV class="subtitle">Priority: ${confirm.priority}</DIV>
    <DIV class="subtitle">Tracking number: ${confirm.trackingNumber}</DIV>
</body>
</html>