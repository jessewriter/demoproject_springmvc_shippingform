<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" type="text/css" href="/resources/css/coreStyle.css" /> -->
<title>Jits Carrier</title>
</head>

<body>
<c:import url="Header.jsp" />
	<!-- no slash current directoy / go to root -->

	<a href="home">home</a>

	<SPAN class="subtitle">Input Delivery Details -
		${deliveryPO.carrier}</SPAN>


	<form:form commandName="deliveryPO"
		action="/web2/carrier/createDelivery" method="POST">
		<form:errors path="*" cssClass="errorblock" element="div" />

		<DIV class="pad">
			<SPAN class="spanClass"> <c:out value="Priority Options" /></SPAN> <SPAN
				class="spanClass"> <form:select path="priority">
					<form:option value="-" label="--Please Select--" />
					<form:options items="${carrier.tariffs}" itemLabel="duration" />

				</form:select>
			</SPAN> <SPAN class="spanClass"> <c:out value="Delivery Type" /></SPAN> <SPAN>
				<form:select path="type">
					<form:option value="-" label="--Please Select--" />
					<form:option value="AIR" itemLabel="AIR" />
					<form:option value="GROUND" itemLabel="GROUND" />
				</form:select>
			</SPAN>

		</DIV>
		<DIV class="pad">
			<SPAN class="spanClass"> <c:out value="Weight" /></SPAN> <SPAN
				class="spanClass"><form:input path="weight" /> </SPAN> <SPAN
				class="spanClass"> <c:out value="Discount Code" /></SPAN> <SPAN>
				<form:input path="discountCode" />
			</SPAN>

		</DIV>
		<DIV class="divPad">
			<DIV class="pad">
				<DIV>
					<SPAN class="spanClass"> Origin Street</SPAN> <SPAN> <form:input
							path="originStreet" /></SPAN>
				</DIV>
				<DIV>
					<SPAN class="spanClass"> Origin City</SPAN> <SPAN> <form:input
							path="originCity" /></SPAN>
				</DIV>
				<DIV>
					<SPAN class="spanClass"> Origin State</SPAN> <SPAN> <form:input
							path="originState" /></SPAN>
				</DIV>
				<DIV>
					<SPAN class="spanClass"> Origin Zip</SPAN> <SPAN> <form:input
							path="originZipcode" /></SPAN>
				</DIV>
			</DIV>
		</DIV>
		<DIV class="divPad">
			<DIV class="pad">
				<DIV>
					<SPAN class="spanClass"> Destination Street</SPAN> <SPAN> <form:input
							path="destStreet" /></SPAN>
				</DIV>
				<DIV>
					<SPAN class="spanClass"> Destination City</SPAN> <SPAN> <form:input
							path="destCity" /></SPAN>
				</DIV>
				<DIV>
					<SPAN class="spanClass"> Destination State</SPAN> <SPAN> <form:input
							path="destState" /></SPAN>
				</DIV>
				<DIV>
					<SPAN class="spanClass"> Destination Zip</SPAN> <SPAN> <form:input
							path="destZipcode" /></SPAN>
				</DIV>
			</DIV>
		</DIV>

		<DIV class="pad">
			<input type="submit" value="Submit Delivery" class="carrierSubmit" />
			<form:hidden path="carrier" />
		</DIV>
	</form:form>
</body>
</html>