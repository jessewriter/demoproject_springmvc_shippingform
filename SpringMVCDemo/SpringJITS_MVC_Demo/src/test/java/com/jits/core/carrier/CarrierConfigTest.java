package com.jits.core.carrier;

 
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jits.core.DeliveryPriorityType;
import com.jits.core.carrier.PostalCarrier;


@RunWith(SpringJUnit4ClassRunner.class)
//specifies the Spring configuration to load for this test fixture
@ContextConfiguration  // apparently works without a path
public class CarrierConfigTest {
	
	@Autowired
	@Qualifier("uex")
	private PostalCarrier unitedExpress;
	
	@Autowired 
	@Qualifier("unitedExpressBean") // no xml
	private PostalCarrier unitedExpress2;
	
	@Autowired 
	@Qualifier("fps")
	private PostalCarrier federalPostalService;
	
	@Autowired 
	@Qualifier("federalPSBean")  // no xml
	private PostalCarrier federalPostalService2;
	
	
 
	@Before 
	public void setUp(){
 	 
	}
	
	@Test
	public void testUnitedExpress(){
		double first =  unitedExpress.retrieveTariffFactor(DeliveryPriorityType.FIRST);
		assertEquals("FIRST", 0.3 , first, 0.001);
		double second =  unitedExpress.retrieveTariffFactor(DeliveryPriorityType.SECOND);
		assertEquals("SECOND", 0.15 , second, 0.001);
		double third =  unitedExpress.retrieveTariffFactor(DeliveryPriorityType.WHENEVER);
		assertEquals("WHENEVER", 0.05 , third, 0.001);	
	}
	
	@Test
	public void testUnitedExpressWithoutXML(){
		double first =  unitedExpress2.retrieveTariffFactor(DeliveryPriorityType.FIRST);
		assertEquals("FIRST", 0.3 , first, 0.001);
		double second =  unitedExpress2.retrieveTariffFactor(DeliveryPriorityType.SECOND);
		assertEquals("SECOND", 0.15 , second, 0.001);
		double third =  unitedExpress2.retrieveTariffFactor(DeliveryPriorityType.WHENEVER);
		assertEquals("WHENEVER", 0.05 , third, 0.001);	
	}
	
	
	 
	@Test
	public void testFederalPostalService(){
		double first =  federalPostalService.retrieveTariffFactor(DeliveryPriorityType.FIRST);
		assertEquals("FIRST", 0.44 , first, 0.001);
		double second =  federalPostalService.retrieveTariffFactor(DeliveryPriorityType.SECOND);
		assertEquals("SECOND", 0.15 , second, 0.001);
		double third =  federalPostalService.retrieveTariffFactor(DeliveryPriorityType.WHENEVER);
		assertEquals("WHENEVER", 0.02 , third, 0.001);	
	} 
	
	@Test
	public void testFederalPostalServiceWithoutXML(){
		double first =  federalPostalService2.retrieveTariffFactor(DeliveryPriorityType.FIRST);
		assertEquals("FIRST", 0.44 , first, 0.001);
		double second =  federalPostalService2.retrieveTariffFactor(DeliveryPriorityType.SECOND);
		assertEquals("SECOND", 0.15 , second, 0.001);
		double third =  federalPostalService2.retrieveTariffFactor(DeliveryPriorityType.WHENEVER);
		assertEquals("WHENEVER", 0.02 , third, 0.001);	
	} 
}
