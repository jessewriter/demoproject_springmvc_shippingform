package com.jits.core.carrier;

 
import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
//specifies the Spring configuration to load for this test fixture
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/root-context.xml"})
public class EnumTest {
	
	@Resource 
	private String x;
	 
	@Test
	public void testUnitedExpress(){
		System.out.println(x); 
		 
	}
	 
	 
}
