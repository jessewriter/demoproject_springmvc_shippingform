package com.jits.core.kiosk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jits.core.JitsDelivery;
import com.jits.core.insurance.DeliveryInsurance;
import com.jits.core.insurance.Insurable;
import com.jits.web.DeliveryPO;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/root-context.xml", "file:src/main/webapp/WEB-INF/spring/insurance-context.xml",
		"file:src/main/resources/com/jits/core/discounts/corporate-context.xml"})
public class KioskConfirmationTest {
//	/PL4_Lab03/src/main/webapp/WEB-INF/spring/root-context.xml
	// /PL4_Lab03/src/main/resources/com/jits/core/discounts/corporate-context.xml
	@Autowired
	KioskImp kiosk;
	
	private    DeliveryPO po;
	
	@Before
	public void createairDelivery(){
		// Address origin = new Address();
		po = new DeliveryPO();
		 po.setOriginCity("Dallas");
		 po.setOriginState("TX");
		 po.setOriginStreet("123 Main");
		 po.setOriginZipcode("75025");		 
		 
		// Address dest = new Address();
		 po.setDestCity("Atlanta");
		 po.setDestState("GA");
		 po.setDestStreet("123 Cobb Road");
		 po.setDestZipcode("68699");
	 
		 double weight = 67.00;
		 po.setCarrier("Federal Postal Service");  // must be the name given in the bean
		 po.setPriority("FIRST");
		 po.setType("AIR");
		 po.setWeight(weight);
	}
	
	@Test
	public void testFirstAir() throws FileNotFoundException {
		assertNotNull(kiosk);
		for(Insurable ins: kiosk.getInsurables()){
			System.out.println("filtered ins list " +ins.getFactor());
		}
		
		System.out.println("carrier  " +kiosk.getCarrier("Federal Postal Service"));
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		System.out.println("jits cost " + jits.getCost());
		assertEquals(1400.3, jits.getCost(), .01);
		System.out.println("jits discount" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
		System.out.println("kiosk confirm called " +kiosk.confirm().getCarrierDescription());
	}
	
	
	
	@Test
	public void unitedExpressWithIns() throws Exception {
		po.setPriority("SECOND");
		po.setType("GROUND");
		po.setCarrier("United Express");
		
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		jits.setInsurance(new DeliveryInsurance());
		System.out.println("jits cost United E SECONDgROUND " + jits.getCost());
		assertEquals(14.25, jits.getCost(), .01);
		System.out.println("jits discount United U SECONDgROUND" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
		List<Insurable> insurables = kiosk.getInsurables();
		for(Insurable ins: insurables){
			System.out.println("insurables desc " + ins.getDescription());
		}
		kiosk.addInsuranceOption("Certificate Of Mailing");
		
		System.out.println("jits after insurance" + kiosk.getDelivery().getCost());
	}

}
