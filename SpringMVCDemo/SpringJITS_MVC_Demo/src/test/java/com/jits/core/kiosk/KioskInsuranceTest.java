package com.jits.core.kiosk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jits.core.JitsDelivery;
import com.jits.core.insurance.DeliveryInsurance;
import com.jits.core.insurance.Insurable;
import com.jits.web.DeliveryPO;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/root-context.xml", "file:src/main/webapp/WEB-INF/spring/insurance-context.xml",
		"file:src/main/resources/com/jits/core/discounts/corporate-context.xml"})
public class KioskInsuranceTest {
//	/PL4_Lab03/src/main/webapp/WEB-INF/spring/root-context.xml
	// /PL4_Lab03/src/main/resources/com/jits/core/discounts/corporate-context.xml
	@Autowired
	KioskImp kiosk;
	
//	@Resource(name="federalPSBean")
//	FederalPostalService fps;
	
	private    DeliveryPO po;
	
	@Before
	public void createairDelivery(){
		// Address origin = new Address();
		po = new DeliveryPO();
		 po.setOriginCity("Dallas");
		 po.setOriginState("TX");
		 po.setOriginStreet("123 Main");
		 po.setOriginZipcode("75025");		 
		 
		// Address dest = new Address();
		 po.setDestCity("Atlanta");
		 po.setDestState("GA");
		 po.setDestStreet("123 Cobb Road");
		 po.setDestZipcode("68699");
	 
		 double weight = 67.00;
		 po.setCarrier("Federal Postal Service");  // must be the name given in the bean
		 po.setPriority("FIRST");
		 po.setType("AIR");
		 po.setWeight(weight);
	}
	
	@Test
	public void testFirstAir() {
		assertNotNull(kiosk);
		for(Insurable ins: kiosk.getInsurables()){
			System.out.println("filtered ins list " +ins.getFactor());
		}
		
		System.out.println("carrier  " +kiosk.getCarrier("Federal Postal Service"));
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		System.out.println("jits cost " + jits.getCost());
		assertEquals(1400.3, jits.getCost(), .01);
		System.out.println("jits discount" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
		assertEquals("Federal Postal Service", kiosk.getSelectedCarrier().getName());
	}
	
	@Test
	public void secondAir() throws Exception {
		po.setPriority("SECOND");
		po.setType("AIR");
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		System.out.println("jits cost SECONDaIR " + jits.getCost());
		assertEquals(477.375, jits.getCost(), .01);
		System.out.println("jits discount SECONDaIR" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
	}
	
	@Test
	public void firstGround() throws Exception {
		po.setPriority("FIRST");
		po.setType("GROUND");
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		System.out.println("jits cost FIRSTgROUND " + jits.getCost());
		assertEquals(41.8, jits.getCost(), .01);
		System.out.println("jits discount FIRSTgROUND" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
	}
	
	@Test
	public void secondGround() throws Exception {
		po.setPriority("SECOND");
		po.setType("GROUND");
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		System.out.println("jits cost SECONDgROUND " + jits.getCost());
		assertEquals(14.25, jits.getCost(), .01);
		System.out.println("jits discount SECONDgROUND" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
	}
	
	@Test
	public void unitedExpress() throws Exception {
		po.setPriority("SECOND");
		po.setType("GROUND");
		po.setCarrier("United Express");
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		System.out.println("jits cost United E SECONDgROUND " + jits.getCost());
		assertEquals(14.25, jits.getCost(), .01);
		System.out.println("jits discount United U SECONDgROUND" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
	}
	
	@Test
	public void unitedExpressWithIns() throws Exception {
		po.setPriority("SECOND");
		po.setType("GROUND");
		po.setCarrier("United Express");
		
		JitsDelivery jits= kiosk.createJitsDelivery(po);
		jits.setInsurance(new DeliveryInsurance());
		System.out.println("jits cost United E SECONDgROUND " + jits.getCost());
		assertEquals(14.25, jits.getCost(), .01);
		System.out.println("jits discount United U SECONDgROUND" + jits.getDiscount().getAmount());
		assertEquals(0.05, jits.getDiscount().getAmount(), .01);
		List<Insurable> insurables = kiosk.getInsurables();
		for(Insurable ins: insurables){
			System.out.println("insurables desc " + ins.getDescription());
		}
		kiosk.addInsuranceOption("Certificate Of Mailing");
		
		System.out.println("jits after insurance" + kiosk.getDelivery().getCost());
		/*insurables desc Certificate Of Mailing
		insurables desc Signature Confirmation
		insurables desc Restricted Delivery*/
		assertTrue(insurables.size()==3);
		//can get the stored Jits in kiosk
		assertEquals("United Express", kiosk.getDelivery().getCarrier().getName());
		//cost after insurance applied is more
		assertEquals(14.9625, jits.getCost(), .01);
	}

}
