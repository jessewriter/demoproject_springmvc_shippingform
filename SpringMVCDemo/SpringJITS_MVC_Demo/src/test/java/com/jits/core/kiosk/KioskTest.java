package com.jits.core.kiosk;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.corporate.core.Discount;
import com.jits.core.carrier.Carrier;
import com.jits.core.carrier.Tariff;
import com.jits.core.carrier.UnitedExpress;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/root-context.xml", "file:src/main/webapp/WEB-INF/spring/insurance-context.xml",
"file:src/main/resources/com/jits/core/discounts/corporate-context.xml"})
public class KioskTest {
//	/PL4_Lab02/src/main/webapp/WEB-INF/spring/root-context.xml
	@Autowired  // since it is filter included no @Component needed on the class
	KioskImp kioskImp;

	@Test
	public void visualTest() {
		// discount and carrier have no toString()
		List<Discount> discounts = kioskImp.getDiscounts();
		for (Discount disc: discounts) {
			System.out.println("+++++++++++++++" + disc.getAmount() + " code: " + disc.getCode());
		}
		List<Carrier> carriers= kioskImp.getCarriers();
		for (Carrier carrier: carriers) {
			System.out.println("++++++++++carrier+++++" + carrier.getName());  //3 fed 1 united
			for(Tariff values: carrier.getTariffs().values()){
				 System.out.println("tariff factor: " +values.getFactor());
			}
		}
		
	}
	
	@Test
	public void kioskTestGetDiscount() throws Exception {
		assertEquals(0.25, kioskImp.getDiscount("ABC25").getAmount(), .01);
		assertEquals(0.05, kioskImp.getDiscount("ABC05").getAmount(), .01);
	}
	
	@Test
	public void kioskGetCarriers() throws Exception {
		assertEquals(4, kioskImp.getCarriers().size());
	}
	
	@Test
	public void kioskGetDiscounts() throws Exception {
		assertEquals(6, kioskImp.getDiscounts().size());
	}
	
	@Test
	public void kioskCodeUpdated() throws Exception {
		//kioskImp.
		
	}
	
	@Test
	public void getCarrier() throws Exception {
		System.out.println("getCarrier= " + kioskImp.getCarrier("United Express"));
		assertTrue(kioskImp.getCarrier("United Express") instanceof UnitedExpress);
	}

}
